using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class EpiRepository : BaseRepository<Epi>, IEpiRepository
    {
        public EpiRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<IEnumerable<Epi>> GetAllAsync()
        {
            IQueryable<Epi> epis = DbSet
                .Include(e => e.ColaboradoresEpis)
                .ThenInclude(ce => ce.Epi);

            return await epis.ToListAsync();
        }

        public override async Task<Epi> GetByIdAsync(int id)
        {
            IQueryable<Epi> epis = DbSet.Where(e => e.Id == id)
                .Include(e => e.ColaboradoresEpis)
                .ThenInclude(ce => ce.Epi);

            return await epis.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Epi>> GetByNameAsync(string name)
        {
            IQueryable<Epi> epis = DbSet.Where(e => e.Nome.ToLower().Contains(name.ToLower()))
                .Include(e => e.ColaboradoresEpis)
                .ThenInclude(ce => ce.Epi);

            return await epis.ToListAsync();
        }
    }
}