using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class ColaboradorRepository : BaseRepository<Colaborador>, IColaboradorRepository
    {
        public ColaboradorRepository(ControlEpiContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Colaborador>> GetAllAsync()
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Include(c => c.Setor)
                .Include(c => c.ColaboradoresEpis)
                .ThenInclude(ce => ce.Colaborador);

                return await colaboradores.ToListAsync();
        }

        public override async Task<Colaborador> GetByIdAsync(int id)
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Include(c => c.Setor)
                .Include(c => c.ColaboradoresEpis)
                .ThenInclude(ce => ce.Colaborador)
                .Where(c => c.Id == id);

            return await colaboradores.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Colaborador>> GetByNameAsync(string name)
        {
            IQueryable<Colaborador> colaboradores = DbSet
                .Include(c => c.Setor)
                .Include(c => c.ColaboradoresEpis)
                .ThenInclude(ce => ce.Colaborador)
                .Where(c => c.Nome.ToLower().Contains(name.ToLower()));

            return await colaboradores.ToListAsync();
        }
    }
}