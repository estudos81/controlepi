using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using ControlEpi.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Implementations
{
    public class EmpresaRepository : BaseRepository<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(ControlEpiContext context) : base(context)
        {}

        public async Task<IEnumerable<Empresa>> GetAllAsync()
        {
            IQueryable<Empresa> empresas = DbSet;

            return await empresas.ToListAsync();
        }

        public override async Task<Empresa> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<IEnumerable<Empresa>> GetByRazaoSocialAsync(string razaoSocial)
        {
            IQueryable<Empresa> empresas = DbSet.Where(s => s.RazaoSocial.ToLower().Contains(razaoSocial.ToLower()));

            return await empresas.ToListAsync();
        }

        public async Task<Empresa> GetUnicaAsync()
        {
            return await DbSet.FirstOrDefaultAsync();
        }
    }
}