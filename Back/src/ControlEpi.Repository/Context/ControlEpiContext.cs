using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ControlEpi.Repository.Context
{
    public class ControlEpiContext : DbContext
    {
        public ControlEpiContext(DbContextOptions<ControlEpiContext> options) : base(options)
        { }

        public DbSet<Colaborador> Colaboradores { get; set; }
        public DbSet<ColaboradorEpi> ColaboradoresEpis { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Epi> Epis { get; set; }
        public DbSet<Setor> Setores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ColaboradorEpi>().HasKey(ce => new { ce.ColaboradorId, ce.EpiId });
            //modelBuilder.Entity<ColaboradorEpi>().HasKey(ce => ce.Id);

            //modelBuilder.Entity<ColaboradorEpi>()
            //    .Property(e => e.Id)
            //    .ValueGeneratedOnAdd();

            //Para cascade delete
            //modelBuilder.Entity<Colaborador>()
            //    .HasMany(c => c.ColaboradoresEpis)
            //    .WithOne(ce => ce.Colaborador)
            //    .OnDelete(DeleteBehavior.Cascade);
        }
        
    }
}