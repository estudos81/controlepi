﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlEpi.Repository.Migrations
{
    public partial class InclusaoPrazoValidadeEpi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PrazoValidadeDias",
                table: "Epis",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrazoValidadeDias",
                table: "Epis");
        }
    }
}
