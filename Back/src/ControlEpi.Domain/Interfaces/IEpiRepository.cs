using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;

namespace ControlEpi.Domain.Interfaces
{
    public interface IEpiRepository : IBaseRepository<Epi>
    {
        Task<IEnumerable<Epi>> GetAllAsync();
        Task<IEnumerable<Epi>> GetByNameAsync(string name);
    }
}