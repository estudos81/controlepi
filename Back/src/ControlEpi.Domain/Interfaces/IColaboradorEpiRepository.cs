using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;

namespace ControlEpi.Domain.Interfaces
{
    public interface IColaboradorEpiRepository : IBaseRepository<ColaboradorEpi>
    {
        Task<ColaboradorEpi[]> GetColaboradorEpiByColaboradorIdAsync(int colaboradorId);
        /// <summary>
        /// Método que retorna apenas 1 ColaboradorEpi
        /// </summary>
        /// <param name="colaboradorId">ID da tabela Colaborador</param>
        /// <param name="id">ID da tabela ColaboradorEpi</param>
        /// <returns>Array de ColaboradorEpis</returns>
    }
}