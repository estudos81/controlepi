using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;

namespace ControlEpi.Domain.Interfaces
{
    public interface IEmpresaRepository : IBaseRepository<Empresa>
    {
        Task<IEnumerable<Empresa>> GetAllAsync();
        Task<IEnumerable<Empresa>> GetByRazaoSocialAsync(string razaoSocial);
        Task<Empresa> GetUnicaAsync();
    }
}