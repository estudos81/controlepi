using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;

namespace ControlEpi.Domain.Interfaces
{
    public interface IColaboradorRepository : IBaseRepository<Colaborador>
    {
        Task<IEnumerable<Colaborador>> GetAllAsync();
        Task<IEnumerable<Colaborador>> GetByNameAsync(string name);
    }
}