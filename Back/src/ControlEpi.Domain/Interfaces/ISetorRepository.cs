using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;

namespace ControlEpi.Domain.Interfaces
{
    public interface ISetorRepository : IBaseRepository<Setor>
    {
        Task<IEnumerable<Setor>> GetAllAsync();
        Task<IEnumerable<Setor>> GetByNameAsync(string name);
    }
}