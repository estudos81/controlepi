using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Domain.Entities
{
    public class Empresa
    {
        public int Id { get; set; }
        public string RazaoSocial { get; set; }
        public string Cnpj { get; set; }
        public string InscrEst { get; set; }
        public string Endereco { get; set; }
        public string Municipio { get; set; }
        public string Uf { get; set; }
        public string Cep { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }
}