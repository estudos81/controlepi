using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Interfaces
{
    public interface IColaboradorAppService
    {
        Task<ColaboradorDto> Add(ColaboradorDto entity);
        Task<ColaboradorDto> Update(int id, ColaboradorDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<ColaboradorDto> entities);
        Task<IEnumerable<ColaboradorDto>> GetAllAsync();
        Task<ColaboradorDto> GetByIdAsync(int id);
        Task<IEnumerable<ColaboradorDto>> GetByNameAsync(string name);
    }
}