using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Interfaces
{
    public interface IEmpresaAppService
    {
        Task<EmpresaDto> Add(EmpresaDto entity);
        Task<EmpresaDto> Update(int id, EmpresaDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<EmpresaDto> entities);
        Task<IEnumerable<EmpresaDto>> GetAllAsync();
        Task<EmpresaDto> GetByIdAsync(int id);
        Task<EmpresaDto> GetUnicaAsync();
        Task<IEnumerable<EmpresaDto>> GetByRazaoSocialAsync(string razaoSocial);
    }
}