using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Interfaces
{
    public interface ISetorAppService
    {
        Task<SetorDto> Add(SetorDto entity);
        Task<SetorDto> Update(int id, SetorDto entity);
        Task<bool> Delete(int id);
        Task<bool> DeleteRange(IEnumerable<SetorDto> entities);
        Task<IEnumerable<SetorDto>> GetAllAsync();
        Task<SetorDto> GetByIdAsync(int id);
        Task<IEnumerable<SetorDto>> GetByNameAsync(string name);
    }
}