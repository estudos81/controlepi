using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Application.Dtos
{
    public class SetorDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}