using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlEpi.Application.Dtos
{
    public class ColaboradorEpiDto
    {
        public int Id { get; set; }
        public int ColaboradorId { get; set; }
        public ColaboradorDto Colaborador { get; set; }
        public int EpiId { get; set; }
        public EpiDto Epi { get; set; }
        public DateTime DataHoraEntrega { get; set; }
    }
}