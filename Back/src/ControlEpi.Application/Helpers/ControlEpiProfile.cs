using System;
using AutoMapper;
using ControlEpi.Domain.Entities;
using ControlEpi.Application.Dtos;

namespace ControlEpi.Application.Helpers
{
    public class ControlEpiProfile : Profile
    {
        public ControlEpiProfile()
        {
            CreateMap<Colaborador, ColaboradorDto>().ReverseMap();
            CreateMap<ColaboradorEpi, ColaboradorEpiDto>().ReverseMap();
            CreateMap<Empresa, EmpresaDto>().ReverseMap();
            CreateMap<Epi, EpiDto>().ReverseMap();
            CreateMap<Setor, SetorDto>().ReverseMap();
        }
    }
}