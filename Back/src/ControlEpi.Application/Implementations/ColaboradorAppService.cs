using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Interfaces;
using ControlEpi.Application.Dtos;
using ControlEpi.Domain.Entities;
using ControlEpi.Domain.Interfaces;
using AutoMapper;

namespace ControlEpi.Application.Implementations
{
    public class ColaboradorAppService : IColaboradorAppService
    {
        private readonly IColaboradorRepository _repository;
        private readonly IMapper _mapper;

        public ColaboradorAppService(IColaboradorRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ColaboradorDto> Add(ColaboradorDto entity)
        {
            var  entityDomain = _mapper.Map<Colaborador>(entity);

            _repository.Add(entityDomain);

            if(await _repository.SaveChangesAsync()){
                var id = (int)entityDomain.GetType().GetProperty("Id").GetValue(entityDomain);

                entityDomain = await _repository.GetByIdAsync(id);

                var entityView = _mapper.Map<ColaboradorDto>(entityDomain);

                return entityView;
            }

            return entity;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await _repository.GetByIdAsync(id);

            if(entity == null) throw new KeyNotFoundException("Colaborador não encontrado");

            _repository.Delete(entity);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteRange(IEnumerable<ColaboradorDto> entities)
        {
            var  entitiesDomain = _mapper.Map<IEnumerable<Colaborador>>(entities);

            _repository.DeleteRange(entitiesDomain);

            if(await _repository.SaveChangesAsync()){
                return true;
            }

            return false;
        }

        public async Task<ColaboradorDto> Update(int id, ColaboradorDto entity)
        {
            var colaborador = await _repository.GetByIdAsync(id);

            if(colaborador == null) return null;

            entity.Id = colaborador.Id;

            _mapper.Map(entity, colaborador);

            _repository.Update(colaborador);

            if(await _repository.SaveChangesAsync()){
                var colaboradorRetorno = await _repository.GetByIdAsync(colaborador.Id);

                var colaboradorView = _mapper.Map<ColaboradorDto>(colaboradorRetorno);
                
                return colaboradorView;
            }

            return null;
        }

        public async Task<IEnumerable<ColaboradorDto>> GetAllAsync()
        {
            var colaboradores = await _repository.GetAllAsync();

            if(colaboradores == null) return null;

            var result = _mapper.Map<IEnumerable<ColaboradorDto>>(colaboradores);

            return result;
        }

        public async Task<ColaboradorDto> GetByIdAsync(int id)
        {
            var colaborador = await _repository.GetByIdAsync(id);

            if(colaborador == null) return null;

            var result = _mapper.Map<ColaboradorDto>(colaborador);

            return result;
        }

        public async Task<IEnumerable<ColaboradorDto>> GetByNameAsync(string name)
        {
            var colaboradores = await _repository.GetByNameAsync(name);

            if(colaboradores == null) return null;

            var result = _mapper.Map<IEnumerable<ColaboradorDto>>(colaboradores);

            return result;
        }
    }
}