using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlEpi.Application.Dtos;
using ControlEpi.Application.Interfaces;
using ControlEpi.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ControlEpi.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EpisController : Controller
    {
        private readonly IEpiAppService _appService;

        public EpisController(IEpiAppService appService)
        {
            _appService = appService;
        }

         [HttpGet]
        public async Task<IActionResult> Get()
        {
            var epis = await _appService.GetAllAsync();

            if(epis == null)
            {
                return NotFound("Nenhum epi encontrado");
            }

            return Ok(epis);    
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var epi = await _appService.GetByIdAsync(id);

            if(epi == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epi);    
        }    

        [HttpGet("{name}/name")]
        public async Task<IActionResult> GetByName(string name)
        {
            var epi = await _appService.GetByNameAsync(name);

            if(epi == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epi);    
        } 

        [HttpPost]
        public async Task<IActionResult> Post(EpiDto epi)
        {
            var epiNew = await _appService.Add(epi);

            if(epiNew == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epiNew);    
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, EpiDto epi)
        {
            var epiAlt = await _appService.Update(id, epi);

            if(epiAlt == null)
            {
                return NotFound("EPI não encontrado");
            }

            return Ok(epiAlt);    
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                 if(await _appService.Delete(id))
                 {
                    return Ok("Deletado com Sucesso!");
                 }
                 else
                 {
                    return BadRequest("Falha ao tentar deleção.");
                 }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Erro: { ex.Message }");
            } 
        }      
    }
}