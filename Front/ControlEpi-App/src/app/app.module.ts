import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
//consumir api
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SetoresComponent } from './components/setores/setores.component';
import { EpisComponent } from './components/epis/epis.component';
import { NavComponent } from './shared/nav/nav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SetorService } from './services/setor.service';
import { EpiService } from './services/epi.service';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
import { ColaboradorService } from './services/colaborador.service';
import { EmpresaComponent } from './components/user/empresa/empresa.component';
import { EmpresaService } from './services/empresa.service';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { TituloComponent } from './shared/titulo/titulo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SetorDetalheComponent } from './components/setores/setor-detalhe/setor-detalhe.component';
import { SetorListaComponent } from './components/setores/setor-lista/setor-lista.component';
import { EpiDetalheComponent } from './components/epis/epi-detalhe/epi-detalhe.component';
import { EpiListaComponent } from './components/epis/epi-lista/epi-lista.component';
import { ColaboradorDetalheComponent } from './components/colaboradores/colaborador-detalhe/colaborador-detalhe.component';
import { ColaboradorListaComponent } from './components/colaboradores/colaborador-lista/colaborador-lista.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegistrationComponent } from './components/user/registration/registration.component';
import { ColaboradorEpiService } from './services/colaborador-epi.service';
import { ColaboradorEpiComponent } from './components/colaborador-epi/colaborador-epi.component';

@NgModule({
  declarations: [
    AppComponent,
    SetoresComponent,
    EpisComponent,
    NavComponent,
    ColaboradoresComponent,
    EmpresaComponent,
    PerfilComponent,
    DashboardComponent,
    TituloComponent,
    SetorDetalheComponent,
    SetorListaComponent,
    EpiDetalheComponent,
    EpiListaComponent,
    ColaboradorDetalheComponent,
    ColaboradorListaComponent,
    UserComponent,
    LoginComponent,
    RegistrationComponent,
    ColaboradorEpiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //consumir api
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CollapseModule.forRoot(),
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      progressBar: true
    })
  ],
  providers: [
    SetorService,
    EpiService,
    ColaboradorService,
    EmpresaService,
    ColaboradorEpiService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
