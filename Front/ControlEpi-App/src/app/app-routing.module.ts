import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
import { EmpresaComponent } from './components/user/empresa/empresa.component';
import { EpisComponent } from './components/epis/epis.component';
import { PerfilComponent } from './components/user/perfil/perfil.component';
import { SetoresComponent } from './components/setores/setores.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SetorDetalheComponent } from './components/setores/setor-detalhe/setor-detalhe.component';
import { SetorListaComponent } from './components/setores/setor-lista/setor-lista.component';
import { EpiListaComponent } from './components/epis/epi-lista/epi-lista.component';
import { EpiDetalheComponent } from './components/epis/epi-detalhe/epi-detalhe.component';
import { ColaboradorListaComponent } from './components/colaboradores/colaborador-lista/colaborador-lista.component';
import { ColaboradorDetalheComponent } from './components/colaboradores/colaborador-detalhe/colaborador-detalhe.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegistrationComponent } from './components/user/registration/registration.component';

const routes: Routes = [
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'registration', component: RegistrationComponent }
    ]
  },
  {
    path: 'setores', component: SetoresComponent,
    children:[
      { path: 'detalhe/:id', component: SetorDetalheComponent },
      { path: 'detalhe', component: SetorDetalheComponent },
      { path: 'lista', component: SetorListaComponent },
    ]
  },
  {
    path: 'colaboradores', component: ColaboradoresComponent,
    children:[
      { path: 'detalhe/:id', component: ColaboradorDetalheComponent },
      { path: 'detalhe', component: ColaboradorDetalheComponent },
      { path: 'lista', component: ColaboradorListaComponent },
    ]
  },
  {
    path: 'epis', component: EpisComponent,
    children:[
      { path: 'detalhe/:id', component: EpiDetalheComponent },
      { path: 'detalhe', component: EpiDetalheComponent },
      { path: 'lista', component: EpiListaComponent },
    ]
  },
  {
    path: 'user/empresa', component: EmpresaComponent
  },
  {
    path: 'user/perfil', component: PerfilComponent,
    //children:[
    //  { path: 'detalhe/:id', component: PerfilDetalheComponent },
    //  { path: 'detalhe', component: PerfilDetalheComponent },
    //]
  },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'setores', redirectTo: 'setores/lista' },
  { path: 'colaboradores', redirectTo: 'colaboradores/lista' },
  { path: 'epis', redirectTo: 'epis/lista' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
