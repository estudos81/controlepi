import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Setor } from '../models/Setor';

//passado para providers em app.module.ts
@Injectable(
//{ providedIn: 'root' }
)

export class SetorService {
  baseUrlSetores = 'https://localhost:5001/api/setores';

  constructor(private http: HttpClient) { }

  public getSetores(): Observable<Setor[]> {
    return this.http.get<Setor[]>(this.baseUrlSetores).pipe(take(1));
  }

  public getSetorById(id: number): Observable<Setor> {
    return this.http.get<Setor>(`${ this.baseUrlSetores }/${ id }`).pipe(take(1));
  }

  public getSetoresByName(name: string): Observable<Setor[]> {
    return this.http.get<Setor[]>(`${ this.baseUrlSetores }/${ name }`).pipe(take(1));
  }

  public post(setor: Setor): Observable<Setor> {
    return this.http
      .post<Setor>(this.baseUrlSetores, setor).pipe(take(1));
  }

  public put(setor: Setor): Observable<Setor> {
    return this.http
      .put<Setor>(`${this.baseUrlSetores}/${setor.id}`, setor).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlSetores}/${id}`).pipe(take(1));
  }
}
