import { take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ColaboradorEpi } from '../models/ColaboradorEpi';
import { ColaboradorService } from './colaborador.service';
import { EpiService } from './epi.service';
import { Colaborador } from '../models/Colaborador';
import { Epi } from '../models/Epi';

@Injectable(
  //{ providedIn: 'root' }
)
export class ColaboradorEpiService {
  baseUrlColaboradorEpi = 'https://localhost:5001/api/ColaboradorEpi';

  constructor(
    private http: HttpClient,
    private colaboradorService: ColaboradorService,
    private epiService: EpiService) { }

  public getColaboradoresEpisByColaborador(colaboradorId: number): Observable<ColaboradorEpi[]> {
    return this.http.get<ColaboradorEpi[]>(`${this.baseUrlColaboradorEpi}/${colaboradorId}`).pipe(take(1));
  }

  public post(colaboradorId: number, colaboradorEpi: ColaboradorEpi): Observable<ColaboradorEpi> {
    return this.http
      .post<ColaboradorEpi>(`${this.baseUrlColaboradorEpi}/${colaboradorId}`, colaboradorEpi).pipe(take(1));
  }

  public devolver(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlColaboradorEpi}/${id}/${true}`).pipe(take(1));
  }

  public getColaboradores(): Observable<Colaborador[]>
  {
    return this.colaboradorService.getColaboradores();
  }

  public getEpis(): Observable<Epi[]> {
    return this.epiService.getEpis();
  }
}
