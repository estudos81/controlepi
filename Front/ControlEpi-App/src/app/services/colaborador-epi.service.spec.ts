/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ColaboradorEpiService } from './colaborador-epi.service';

describe('Service: ColaboradorEpi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ColaboradorEpiService]
    });
  });

  it('should ...', inject([ColaboradorEpiService], (service: ColaboradorEpiService) => {
    expect(service).toBeTruthy();
  }));
});
