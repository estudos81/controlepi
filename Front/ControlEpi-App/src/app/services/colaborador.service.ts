import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Colaborador } from '../models/Colaborador';
import { Setor } from '../models/Setor';
import { SetorService } from './setor.service';

@Injectable(
  //{ providedIn: 'root' }
)

export class ColaboradorService {
  baseUrlColaboradores = 'https://localhost:5001/api/colaboradores';

  constructor(private http: HttpClient, private setorService: SetorService) { }

  public getColaboradores(): Observable<Colaborador[]>
  {
    return this.http.get<Colaborador[]>(this.baseUrlColaboradores);
  }

  public getColaboradorById(id: number): Observable<Colaborador>
  {
    return this.http.get<Colaborador>(`${ this.baseUrlColaboradores }/${id}`)
  }

  public getColaboradoresByName(name: string): Observable<Colaborador[]>
  {
    return this.http.get<Colaborador[]>(`${ this.baseUrlColaboradores }/${ name }`)
  }

  public getSetores(): Observable<Setor[]> {
    return this.setorService.getSetores();
  }

  public post(colaborador: Colaborador): Observable<Colaborador> {
    return this.http
      .post<Colaborador>(this.baseUrlColaboradores, colaborador).pipe(take(1));
  }

  public put(colaborador: Colaborador): Observable<Colaborador> {
    return this.http
      .put<Colaborador>(`${this.baseUrlColaboradores}/${colaborador.id}`, colaborador).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlColaboradores}/${id}`).pipe(take(1));
  }
}
