import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Empresa } from '../models/Empresa';

@Injectable()
export class EmpresaService {
  baseUrlEmpresas = 'https://localhost:5001/api/empresa';

  constructor(private http: HttpClient) { }

  public getEmpresa(): Observable<Empresa[]>
  {
    return this.http.get<Empresa[]>(this.baseUrlEmpresas);
  }

  public getEmpresaById(id: number): Observable<Empresa>
  {
    return this.http.get<Empresa>(`${ this.baseUrlEmpresas }/${ id }`);
  }

  public getUnica(): Observable<Empresa>
  {
    return this.http.get<Empresa>(`${ this.baseUrlEmpresas }/get-unica`);
  }

  public getEmpresasByRazaoSocial(razaoSocial: string): Observable<Empresa[]>
  {
    return this.http.get<Empresa[]>(`${ this.baseUrlEmpresas }/${ razaoSocial }`);
  }

  public post(empresa: Empresa): Observable<Empresa> {
    return this.http
      .post<Empresa>(this.baseUrlEmpresas, empresa).pipe(take(1));
  }

  public put(empresa: Empresa): Observable<Empresa> {
    return this.http
      .put<Empresa>(`${this.baseUrlEmpresas}/${empresa.id}`, empresa).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlEmpresas}/${id}`).pipe(take(1));
  }
}
