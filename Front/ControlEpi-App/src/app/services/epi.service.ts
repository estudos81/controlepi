import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Epi } from '../models/Epi';

@Injectable(
//{ providedIn: 'root'}
)
export class EpiService {
  baseUrlEpis = 'https://localhost:5001/api/epis';

  constructor(private http: HttpClient) { }

  public getEpis(): Observable<Epi[]> {
    return this.http.get<Epi[]>(this.baseUrlEpis);
  }

  public getEpiById(id: number): Observable<Epi> {
    return this.http.get<Epi>(`${ this.baseUrlEpis }/${ id }`);
  }

  public getEpisByName(name: string): Observable<Epi[]> {
    return this.http.get<Epi[]>(`${ this.baseUrlEpis }/${ name }`);
  }

  public post(epi: Epi): Observable<Epi> {
    return this.http
      .post<Epi>(this.baseUrlEpis, epi).pipe(take(1));
  }

  public put(epi: Epi): Observable<Epi> {
    return this.http
      .put<Epi>(`${this.baseUrlEpis}/${epi.id}`, epi).pipe(take(1));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrlEpis}/${id}`).pipe(take(1));
  }
}
