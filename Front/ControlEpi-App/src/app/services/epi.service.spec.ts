/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EpiService } from './epi.service';

describe('Service: Epi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EpiService]
    });
  });

  it('should ...', inject([EpiService], (service: EpiService) => {
    expect(service).toBeTruthy();
  }));
});
