import { ColaboradorEpi } from "./ColaboradorEpi";

export interface Epi {
  id: number;
  codigoInterno: string;
  nome: string;
  marca: string;
  custo: number;
  qtdeEstoque: number;
  prazoValidadeDias: number;
  foto: string;
  colaboradoresEpis: ColaboradorEpi[];
}
