export interface ColaboradorEpi {
  id: number;
  colaboradorId: number;
  epiId: number;
  dataHoraEntrega: Date;
  devolvido: boolean;
}
