import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Setor } from 'src/app/models/Setor';
import { SetorService } from 'src/app/services/setor.service';

@Component({
  selector: 'app-setor-lista',
  templateUrl: './setor-lista.component.html',
  styleUrls: ['./setor-lista.component.scss']
})
export class SetorListaComponent implements OnInit {
  modalRef?: BsModalRef;
  public setores: Setor[] = [];
  public setoresFiltrados: Setor[] = [];
  private _filtroLista: string = '';

  public setorId = 0;

  public get filtroLista(): string
  {
      return this._filtroLista;
  }

  public set filtroLista(value: string)
  {
      this._filtroLista = value;
      this.setoresFiltrados = this.filtroLista ? this.filtrarSetores(this.filtroLista) : this.setores;
  }

  public filtrarSetores(filtro: string): Setor[]
  {
    filtro = filtro.toLocaleLowerCase();

    return this.setores.filter(setor => setor.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  }

  constructor(
    private setorService: SetorService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    this.getSetores();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  public getSetores(): void {
    this.setorService.getSetores().subscribe({
      next: (setores: Setor[]) =>{
        this.setores = setores;
        this.setoresFiltrados = this.setores;
      },
      error: () => {
        this.spinner.hide();
        this.toastr.error('Falha ao carregar setores','Falha');
      },
      complete: () => this.spinner.hide()
    });
  }

  confirm(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.setorService.delete(this.setorId).subscribe(
      (result: any) => {
        console.log(result);
        if(result.message === 'Deletado') {
          this.toastr.success('Setor deletado com sucesso!', 'Sucesso');
          this.spinner.hide();
          this.getSetores();
        }
      },
      (error: any) => {
        console.error(error);
        this.toastr.error('Falha ao Excluir Setor', 'Falha');
        this.spinner.hide();
      },
      () => this.spinner.hide()
    );


  }

  decline(): void {
    this.modalRef?.hide();
  }

  //Caso o redirecionamento para edição esteja na table (bom pra celulares)
  //Se for usar colocar injeção de dependência para Router no constructor: private router: Router
  //detalheEvento(id: number): void {
  //  this.router.navigate([`setores/detalhe/${id}`]);
  //}

  detalheSetor(id: number): void {
    this.router.navigate([`setores/detalhe/${id}`]);
  }

  //Antigo
  //openModal(template: TemplateRef<any>): void {
  //  this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  //}
  //Novo, para evitar Propagation qdo edição está em clique de linha
  openModal(event: any, template: TemplateRef<any>, setorId: number): void {
    event.stopPropagation();
    this.setorId = setorId;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
}
