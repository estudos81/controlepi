import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColaboradorEpiComponent } from './colaborador-epi.component';

describe('ColaboradorEpiComponent', () => {
  let component: ColaboradorEpiComponent;
  let fixture: ComponentFixture<ColaboradorEpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColaboradorEpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColaboradorEpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
