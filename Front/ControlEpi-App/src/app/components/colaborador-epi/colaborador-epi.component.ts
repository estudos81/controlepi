import { ColaboradorEpiService } from './../../services/colaborador-epi.service';
import { Component, OnInit } from '@angular/core';
import { Epi } from 'src/app/models/Epi';
import { ToastrService } from 'ngx-toastr';
import { Colaborador } from 'src/app/models/Colaborador';

@Component({
  selector: 'app-colaborador-epi',
  templateUrl: './colaborador-epi.component.html',
  styleUrls: ['./colaborador-epi.component.scss']
})
export class ColaboradorEpiComponent implements OnInit {

  epis: Epi[];
  colaboradores: Colaborador[];

  constructor(
    private colaboradorEpiService: ColaboradorEpiService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarEpis();
    this.carregarColaboradores();
  }

  public carregarEpis(): void {
    this.colaboradorEpiService.getEpis().subscribe({
      next: (epis: Epi[]) => {
        this.epis = epis;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar EPIs', 'Falha');
        console.error(error);
      }
    });
  }

  public carregarColaboradores(): void {
    this.colaboradorEpiService.getColaboradores().subscribe({
      next: (colaboradores: Colaborador[]) => {
        this.colaboradores = colaboradores;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar Colaboradores', 'Falha');
        console.error(error);
      }
    });
  }
}
