import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Epi } from 'src/app/models/Epi';
import { EpiService } from 'src/app/services/epi.service';


@Component({
  selector: 'app-epi-detalhe',
  templateUrl: './epi-detalhe.component.html',
  styleUrls: ['./epi-detalhe.component.scss']
})
export class EpiDetalheComponent implements OnInit {

  form!: FormGroup;

  epi = {} as Epi;

  tipoSalvamento = 'post';

  get f(): any {
    return this.form.controls;
  }

  constructor(
    private fb: FormBuilder,
    private epiService: EpiService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.carregarEpi();
    this.validation();
  }

  public carregarEpi(): void {
    const epiIdParam = this.activatedRouter.snapshot.paramMap.get('id');

    if(epiIdParam !== null){
      this.spinner.show();

      this.tipoSalvamento = 'put';

      this.epiService.getEpiById(+epiIdParam).subscribe({
        next: (epi: Epi) => {
          this.epi = {...epi};
          this.form.patchValue(this.epi);
        },
        error: (error: any) => {
          this.spinner.hide();
          this.toastr.error('Falha ao carregar EPI', 'Falha');
          console.error(error);
        },
        complete: () => this.spinner.hide()
      });
    }
  }

  public salvarAlteracao(): void {
    this.spinner.show();

    if(this.form.valid) {
      this.epi =
        this.tipoSalvamento === 'post'
          ? { ...this.form.value }
          : { id: this.epi.id, ...this.form.value };

      this.epiService[this.tipoSalvamento](this.epi).subscribe(
        (epiRetorno: Epi) => {
          this.toastr.success('EPI salvo com Sucesso!', 'Sucesso');
          this.router.navigate([`epis/detalhe/${epiRetorno.id}`]);
        },
        (error: any) => {
          console.error(error);
          this.toastr.error('Error ao salvar epi', 'Erro');
        }
      ).add(() => this.spinner.hide());


    }
  }

  public validation(): void {
    this.form = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      codigoInterno: [''],
      marca: [''],
      prazoValidadeDias: [''],
      qtdeEstoque: [''],
      custo: [''],
      foto: ['']
    });
  }

  onSubmit(): void {
    if(this.form.invalid) {
      return;
    }
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }
}
