import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Epi } from 'src/app/models/Epi';
import { EpiService } from 'src/app/services/epi.service';

@Component({
  selector: 'app-epi-lista',
  templateUrl: './epi-lista.component.html',
  styleUrls: ['./epi-lista.component.scss']
})
export class EpiListaComponent implements OnInit {
  modalRef?: BsModalRef;
  public largFoto: number = 70;
  public margFoto: number = 2;

  public epis: Epi[] = [];
  public episFiltrados: Epi[] = [];
  private _filtroLista: string = '';

  public epiId = 0;

  public get filtroLista(): string
  {
      return this._filtroLista;
  }

  public set filtroLista(value: string)
  {
    this._filtroLista = value;
    this.episFiltrados = this.filtroLista ? this.filtrarEpis(this.filtroLista) : this.epis;
  }

  public filtrarEpis(filtro: string): Epi[]
  {
    filtro = filtro.toLocaleLowerCase();

    return this.epis.filter(epi => epi.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  }

  constructor(private epiService: EpiService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    this.getEpis();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  public getEpis(): void {
    this.epiService.getEpis().subscribe({
      next: (epis: Epi[]) =>{
        this.epis = epis;
        this.episFiltrados = this.epis;
      },
      error: () => {
        this.spinner.hide();
        this.toastr.error('Falha ao carregar epis','Falha');
      },
      complete: () => this.spinner.hide()
    });
  }

  detalheEpi(id: number): void {
    this.router.navigate([`epis/detalhe/${id}`]);
  }

  confirm(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.epiService.delete(this.epiId).subscribe(
      (result: any) => {
        console.log(result);
        if(result.message === 'Deletado') {
          this.toastr.success('EPI deletado com sucesso!', 'Sucesso');
          this.spinner.hide();
          this.getEpis();
        }
      },
      (error: any) => {
        console.error(error);
        this.toastr.error('Falha ao Excluir EPI', 'Falha');
        this.spinner.hide();
      },
      () => this.spinner.hide()
    );
  }

  decline(): void {
    this.modalRef?.hide();
  }

  //Caso o redirecionamento para edição esteja na table (bom pra celulares)
  //Se for usar colocar injeção de dependência para Router no constructor: private router: Router
  //detalheEvento(id: number): void {
  //  this.router.navigate([`setores/detalhe/${id}`]);
  //}

  //Antigo
  //openModal(template: TemplateRef<any>): void {
  //  this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  //}
  //Novo, para evitar Propagation qdo edição está em clique de linha
  openModal(event: any, template: TemplateRef<any>, epiId: number): void {
    event.stopPropagation();
    this.epiId = epiId;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }
}
