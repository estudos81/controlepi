import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/models/Colaborador';
import { ColaboradorService } from 'src/app/services/colaborador.service';

@Component({
  selector: 'app-colaboradores',
  templateUrl: './colaboradores.component.html',
  styleUrls: ['./colaboradores.component.scss']
})
export class ColaboradoresComponent implements OnInit {
  public colaboradores: Colaborador[] = [];
  public colaboradoresFiltrados: Colaborador[] = [];
  private _filtroLista: string = '';

  public get filtroLista(): string
  {
      return this._filtroLista;
  }

  public set filtroLista(value: string)
  {
      this._filtroLista = value;
      this.colaboradoresFiltrados = this.filtroLista ? this.filtrarColaboradores(this.filtroLista) : this.colaboradores;
  }

  public filtrarColaboradores(filtro: string): Colaborador[]
  {
    filtro = filtro.toLocaleLowerCase();

    return this.colaboradores.filter(colaborador => colaborador.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  }

  constructor(private colaboradorService: ColaboradorService) { }

  ngOnInit() {
    this.getColaboradores();
  }

  public getColaboradores(): void {
    this.colaboradorService.getColaboradores().subscribe(
      (_colaboradores: Colaborador[]) => {
        this.colaboradores = _colaboradores;
        this.colaboradoresFiltrados = this.colaboradores;
      },
      error => console.log(error)
    );
  }
}
