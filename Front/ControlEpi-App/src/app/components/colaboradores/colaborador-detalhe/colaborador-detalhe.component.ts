import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ColaboradorEpi } from './../../../models/ColaboradorEpi';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Colaborador } from 'src/app/models/Colaborador';
import { Setor } from 'src/app/models/Setor';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { ColaboradorEpiService } from 'src/app/services/colaborador-epi.service';
import { Epi } from 'src/app/models/Epi';

@Component({
  selector: 'app-colaborador-detalhe',
  templateUrl: './colaborador-detalhe.component.html',
  styleUrls: ['./colaborador-detalhe.component.scss']
})
export class ColaboradorDetalheComponent implements OnInit {
  modalRef: BsModalRef;
  form: FormGroup;
  colaboradorIdParam: number;
  colaborador = {} as Colaborador;
  setores: Setor[];
  epis: Epi[];
  tipoSalvamento = 'post';
  ceAtual = {id: 0, index: 0};

  get modoEditar(): boolean {
    return this.tipoSalvamento === 'put';
  }

  get f(): any {
    return this.form.controls;
  }

  get ces(): FormArray {
    return this.form.get('colaboradoresEpis') as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private colaboradorService: ColaboradorService,
    private colaboradorEpiService: ColaboradorEpiService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private modalService: BsModalService) { }

  ngOnInit(): void {
    this.carregarSetores();
    this.carregarColaborador();

    //this.carregarEpis();
    //this.carregarColaboradoresDropDown();

    this.validation();
  }

  public carregarColaborador(): void {
    this.colaboradorIdParam = +this.activatedRouter.snapshot.paramMap.get('id');

    if(this.colaboradorIdParam !== null && this.colaboradorIdParam !== 0){
      this.spinner.show();

      this.tipoSalvamento = 'put';

      this.colaboradorService.getColaboradorById(this.colaboradorIdParam).subscribe({
        next: (colaborador: Colaborador) => {
          this.colaborador = {...colaborador};
          this.form.patchValue(this.colaborador);
          this.carregarEpis();
          this.colaborador.colaboradoresEpis.forEach(ce => {
            this.ces.push(this.criarColaboradorEpi(ce));
          });
          //caso precise pegar os registros separadamente
          //this.carregarColaboradoresEpis();
        },
        error: (error: any) => {
          this.spinner.hide();
          this.toastr.error('Falha ao carregar Colaborador', 'Falha');
          console.error(error);
        },
        complete: () => this.spinner.hide()
      });
    }
  }

  public carregarColaboradoresEpis(): void {
    this.colaboradorEpiService
      .getColaboradoresEpisByColaborador(this.colaboradorIdParam)
      .subscribe(
        (cesRetorno: ColaboradorEpi[]) => {
          //this.carregarEpis();
          cesRetorno.forEach(ce => {
            this.ces.push(this.criarColaboradorEpi(ce));
          });
        },
        (error: any) => {
          this.toastr.error('Erro ao tentar carregar Epis', 'Erro');
          console.error(error);
        }
      )
      .add(() => this.spinner.hide());
  }

  criarColaboradorEpi(ce: ColaboradorEpi): FormGroup {
    return this.fb.group({
      id: [ce.id],
      colaboradorId: [ce.colaboradorId, Validators.required],
      epiId: [ce.epiId, Validators.required]
    });
  }

  public carregarSetores(): void {
    this.colaboradorService.getSetores().subscribe({
      next: (setores: Setor[]) => {
        this.setores = setores;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar Setores', 'Falha');
        console.error(error);
      }
    });
  }

  public salvarColaborador(): void {
    this.spinner.show();

    if(this.form.valid) {
      this.colaborador =
        this.tipoSalvamento === 'post'
          ? { ...this.form.value }
          : { id: this.colaborador.id, ...this.form.value };

      this.colaboradorService[this.tipoSalvamento](this.colaborador).subscribe(
        (colaboradorRetorno: Colaborador) => {
          this.toastr.success('Colaborador salvo com Sucesso!', 'Sucesso');
          this.router.navigate([`colaboradores/detalhe/${colaboradorRetorno.id}`]);
        },
        (error: any) => {
          console.error(error);
          this.toastr.error('Error ao salvar Colaborador', 'Erro');
        }
      ).add(() => this.spinner.hide());


    }
  }

  public salvarColaboradorEpi(): void {
    if(this.form.controls.colaboradoresEpis.valid){
      this.spinner.show();
      this.colaboradorEpiService.post(this.colaboradorIdParam, this.form.value.colaboradoresEpis)
      .subscribe(
        () => {

          //this.carregarEpis();
          this.ces.removeAt(this.ceAtual.index);
          this.carregarColaboradoresEpis();

          this.toastr.success('EPI salvo com sucesso', 'Sucesso');
          //this.ces.reset();
        },
        (error: any) => {
          this.toastr.error('Falha ao salvar EPI', 'Falha');
          console.error(error);
        }
      ).add(() => this.spinner.hide());
    }
  }

  public validation(): void {
    this.form = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      setorId: [''],
      cpf: [''],
      rg: [''],
      telefone: [''],
      email: [''],
      endereco: [''],
      municipio: [''],
      uf: [''],
      cep: [''],
      colaboradoresEpis: this.fb.array([]),
    });
  }

  adicionarColaboradorEpi(): void {
    this.ces.push(this.criarCe({id: 0, colaboradorId: this.colaboradorIdParam} as ColaboradorEpi));

    this.carregarEpis();
  }

  criarCe(ce: ColaboradorEpi): FormGroup {
    return this.fb.group({
      id: [ce.id],
      colaboradorId: [ce.colaboradorId],
      epiId: [ce.epiId]
  })
  }

  public carregarEpis(): void {
    this.colaboradorEpiService.getEpis().subscribe({
      next: (epis: Epi[]) => {
        this.epis = epis;
        //this.form.patchValue(this.colaborador);
      },
      error: (error: any) => {
        this.toastr.error('Falha ao carregar EPIs', 'Falha');
        console.error(error);
      }
    });
  }

  onSubmit(): void {
    if(this.form.invalid) {
      return;
    }
  }

  public resetForm(event: any): void {
    event.preventDefault();
    this.form.reset();
  }

  public devolverEpi(template: TemplateRef<any>, index: number): void {

    this.ceAtual.id = this.ces.get(index + '.id').value;
    this.ceAtual.index = index;

    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  confirmDevolucao(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.colaboradorEpiService.devolver(this.ceAtual.id).subscribe(
      () => {
          this.ces.removeAt(this.ceAtual.index);
          this.toastr.success('EPI devolvido com sucesso!', 'Sucesso');
      },
      (error: any) => {
        console.error(error);
        this.toastr.error('Falha em devolução de EPI', 'Falha');
        this.spinner.hide();
      }
    ).add(() => this.spinner.hide());
  }

  declineDevolucao(): void {
    this.modalRef?.hide();
  }

  cancelarEntrega(index: number) {
    this.ces.removeAt(index);
  }

  public cssValidator(campoForm: FormControl | AbstractControl): any {
    return { 'is-invalid': campoForm.errors && campoForm.touched };
  }
}
