import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Colaborador } from 'src/app/models/Colaborador';
import { ColaboradorService } from 'src/app/services/colaborador.service';

@Component({
  selector: 'app-colaborador-lista',
  templateUrl: './colaborador-lista.component.html',
  styleUrls: ['./colaborador-lista.component.scss']
})
export class ColaboradorListaComponent implements OnInit {
  modalRef?: BsModalRef;
  public colaboradores: Colaborador[] = [];
  public colaboradoresFiltrados: Colaborador[] = [];
  private _filtroLista: string = '';

  public largFoto: number = 70;
  public margFoto: number = 2;

  public colaboradorId = 0;

  public get filtroLista(): string
  {
      return this._filtroLista;
  }

  public set filtroLista(value: string)
  {
      this._filtroLista = value;
      this.colaboradoresFiltrados = this.filtroLista ? this.filtrarColaboradores(this.filtroLista) : this.colaboradores;
  }

  public filtrarColaboradores(filtro: string): Colaborador[]
  {
    filtro = filtro.toLocaleLowerCase();

    return this.colaboradores.filter(colaborador => colaborador.nome.toLocaleLowerCase().indexOf(filtro) !== -1);
  }

  constructor(private colaboradorService: ColaboradorService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    /** spinner starts on init */
    this.spinner.show();

    this.getColaboradores();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  public getColaboradores(): void {
    this.colaboradorService.getColaboradores().subscribe({
      next: (colaboradores: Colaborador[]) =>{
        this.colaboradores = colaboradores;
        this.colaboradoresFiltrados = this.colaboradores;
      },
      error: () => {
        this.spinner.hide();
        this.toastr.error('Falha ao carregar colaboradores','Falha');
      },
      complete: () => this.spinner.hide()
    });
  }

  detalheColaborador(id: number): void {
    this.router.navigate([`colaboradores/detalhe/${id}`]);
  }

  confirm(): void {
    this.modalRef?.hide();
    this.spinner.show();

    this.colaboradorService.delete(this.colaboradorId).subscribe(
      (result: any) => {
        console.log(result);
        if(result.message === 'Deletado') {
          this.toastr.success('Colaborador deletado com sucesso!', 'Sucesso');
          this.spinner.hide();
          this.getColaboradores();
        }
      },
      (error: any) => {
        console.error(error);
        this.toastr.error('Falha ao Excluir Colaborador', 'Falha');
        this.spinner.hide();
      },
      () => this.spinner.hide()
    );
  }

  decline(): void {
    this.modalRef?.hide();
  }

  //Caso o redirecionamento para edição esteja na table (bom pra celulares)
  //Se for usar colocar injeção de dependência para Router no constructor: private router: Router
  //detalheEvento(id: number): void {
  //  this.router.navigate([`setores/detalhe/${id}`]);
  //}

  //Antigo
  //openModal(template: TemplateRef<any>): void {
  //  this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  //}
  //Novo, para evitar Propagation qdo edição está em clique de linha
  openModal(event: any, template: TemplateRef<any>, colaboradorId: number): void {
    event.stopPropagation();
    this.colaboradorId = colaboradorId;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

}
